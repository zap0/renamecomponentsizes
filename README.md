# README #

This is a standalone implementation. A modular version is available as well. [Repo here](https://bitbucket.org/zap0/aurora-component-values/)

----

A text-changing mod For Aurora C#

Ever been bothered by the inconsistent scheme of the various reduced-size components when fiddling with a design?
Bothered that "Small", "Fighter" and "Tiny" mean different things for Engineering Spaces, Fuel Tanks and Maintenance Storage Bays?
This script appends the actual size of the component to it's name, ending the guessing game.

[Example image](https://i.imgur.com/BLGdjta.png)

This is a simple sql script which will rename 41 components: Cargo holds, Cryo modules, Troop Transport Bays, Engineering Spaces, Fuel Storage, Crew Quarters and Maintenance Storage Bays.
Small component names (Engineering, fuel, maintenance storage, crew quarters) are all renamed to state their size in tons, cargo holds their relative size to the standard 25k cargo hold, cryo units state how many people they can freeze and troop components state their troop capacity in tons (the drop/boarding components are larger than their capacity).

### Examples ###

* Engineering Spaces - Tiny -> Engineering Spaces (12.5t)
* Engineering Spaces - Fighter -> Engineering Spaces (5t)
* Small Maintenance Storage Bay -> Maintenance Storage Bay (10t)
* Tiny Maintenance Storage Bay -> Maintenance Storage Bay (0.5t)
* Fighter Maintenance Storage Bay ->Maintenance Storage Bay (2.5t)
* Cargo Hold - Small -> Cargo Hold - Small (x0.2)
* Cryogenic Transport -> Cryogenic Transport (10k)
* Cryogenic Transport - Emergency -> Cryogenic Transport (200)
* Cryogenic Transport - Small -> Cryogenic Transport (1k)
* Fuel Storage - Fighter -> Fuel Storage (1t)
* Fuel Storage - Tiny -> Fuel Storage (5t)
* Fuel Storage - Large -> Fuel Storage (250t)

As you can see, "Fighter-sized" means three different sizes, depending on the component.
You can also freely edit the names in the sql file in the download to fit another naming scheme.

### Compatibility ###

During version changes there are sometimes components added or removed (e.g. a new size of fuel tanks). If this script has not been updated, the new component will just not be renamed. There shouldn't be any crashes or incompatibilities resulting from using a script made for another game version.

The mod can be added to ongoing games (or removed from one later) and will apply to all games saved in that DB.

Incompatible with other mods that change these component names, as they will just overwrite each other.

### Install ###

Requires sqlite3.exe to be present, linked down below - download sqlite-tools-win32-x86 under Precompiled Binaries for Windows.

To use, put sqlite3.exe and the downloaded files (RenameComponentSizes.bat and .sql) into your Aurora game directory. An uninstall script working the same way to rename the components back to their original names is provided. Once applied these files can be removed.

## Links ##

* [Download for Aurora 2.3+](https://bitbucket.org/zap0/renamecomponentsizes/raw/0dfac03b80ae442631c403dd5e1cd4809b9d48b8/Rename%20Component%20Sizes/RenameComponentSizes-2.3.zip)
* [sqlite download page](https://sqlite.org/download.html) - Required, download sqlite-tools-win32-x86 under Precompiled Binaries for Windows.
* [Reddit Thread](https://old.reddit.com/r/aurora4x_mods/comments/h17gfv/db_script_to_rename_components_engineering_spaces/)
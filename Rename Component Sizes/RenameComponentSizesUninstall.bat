@echo off

if not exist "AuroraDB.db" (
	echo AuroraDB.db not found
	echo Please put this file into your Aurora directory and run it from there.
	echo.
	pause
	exit
)
if not exist "sqlite3.exe" (
	echo sqlite3.exe not found
	echo Please download from sqlite.org and put into this directory.
	echo Look for sqlite-tools-win32-x86 under Precompiled Binaries for Windows.
	echo.
	pause
	exit
)
if not exist "RenameComponentSizesUninstall.sql" (
	echo RenameComponentSizesUninstall.sql not found
	echo It should have come with this file, please put it into this directory.
	echo.
	pause
	exit
)

echo Executing database script on AuroraDB.db...
echo.

sqlite3.exe AuroraDB.db ".read RenameComponentSizesUninstall.sql"

echo.
echo If no errors appeared, the script applied and your components are turned back.
echo You may now delete this file and RenameComponentSizesUninstall.sql.
echo.
pause
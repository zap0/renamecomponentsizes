UPDATE FCT_ShipDesignComponents SET Name='Cargo Hold - Small' WHERE SDComponentID=3;
UPDATE FCT_ShipDesignComponents SET Name='Crew Quarters' WHERE SDComponentID=8;
UPDATE FCT_ShipDesignComponents SET Name='Cryogenic Transport' WHERE SDComponentID=479;
UPDATE FCT_ShipDesignComponents SET Name='Fuel Storage - Standard' WHERE SDComponentID=600;
UPDATE FCT_ShipDesignComponents SET Name='Troop Transport Bay - Large' WHERE SDComponentID=728;
UPDATE FCT_ShipDesignComponents SET Name='Engineering Spaces' WHERE SDComponentID=25147;
UPDATE FCT_ShipDesignComponents SET Name='Crew Quarters - Small' WHERE SDComponentID=26265;
UPDATE FCT_ShipDesignComponents SET Name='Fuel Storage - Small' WHERE SDComponentID=26266;
UPDATE FCT_ShipDesignComponents SET Name='Engineering Spaces - Small' WHERE SDComponentID=26267;
UPDATE FCT_ShipDesignComponents SET Name='Compressed Fuel Storage System' WHERE SDComponentID=26420;
UPDATE FCT_ShipDesignComponents SET Name='Large Maintenance Storage Bay' WHERE SDComponentID=27132;
UPDATE FCT_ShipDesignComponents SET Name='Engineering Spaces - Tiny' WHERE SDComponentID=27133;
UPDATE FCT_ShipDesignComponents SET Name='Engineering Spaces - Fighter' WHERE SDComponentID=27134;
UPDATE FCT_ShipDesignComponents SET Name='Troop Transport Bay - Standard' WHERE SDComponentID=33426;
UPDATE FCT_ShipDesignComponents SET Name='Fuel Storage - Tiny' WHERE SDComponentID=38117;
UPDATE FCT_ShipDesignComponents SET Name='Fuel Storage - Large' WHERE SDComponentID=43529;
UPDATE FCT_ShipDesignComponents SET Name='Fuel Storage - Ultra Large' WHERE SDComponentID=43530;
UPDATE FCT_ShipDesignComponents SET Name='Fuel Storage - Very Large' WHERE SDComponentID=43531;
UPDATE FCT_ShipDesignComponents SET Name='Cryogenic Transport - Emergency' WHERE SDComponentID=43532;
UPDATE FCT_ShipDesignComponents SET Name='Cryogenic Transport - Small' WHERE SDComponentID=43533;
UPDATE FCT_ShipDesignComponents SET Name='Fuel Storage - Minimal' WHERE SDComponentID=43535;
UPDATE FCT_ShipDesignComponents SET Name='Crew Quarters - Tiny' WHERE SDComponentID=47485;
UPDATE FCT_ShipDesignComponents SET Name='Troop Transport Drop Bay - Large' WHERE SDComponentID=55437;
UPDATE FCT_ShipDesignComponents SET Name='Troop Transport Drop Bay - Standard' WHERE SDComponentID=55438;
UPDATE FCT_ShipDesignComponents SET Name='Crew Quarters - Fighter' WHERE SDComponentID=62453;
UPDATE FCT_ShipDesignComponents SET Name='Compressed Fuel Storage System - Large' WHERE SDComponentID=64796;
UPDATE FCT_ShipDesignComponents SET Name='Compressed Fuel Storage System - Very Large' WHERE SDComponentID=64797;
UPDATE FCT_ShipDesignComponents SET Name='Compressed Fuel Storage System - Small' WHERE SDComponentID=65061;
UPDATE FCT_ShipDesignComponents SET Name='Cargo Hold - Tiny' WHERE SDComponentID=65307;
UPDATE FCT_ShipDesignComponents SET Name='Troop Transport Boarding Bay - Standard' WHERE SDComponentID=65454;
UPDATE FCT_ShipDesignComponents SET Name='Troop Transport Boarding Bay - Small' WHERE SDComponentID=65848;
UPDATE FCT_ShipDesignComponents SET Name='Troop Transport Bay - Very Small' WHERE SDComponentID=65849;
UPDATE FCT_ShipDesignComponents SET Name='Troop Transport Drop Bay - Very Small' WHERE SDComponentID=65850;
UPDATE FCT_ShipDesignComponents SET Name='Fuel Storage - Fighter' WHERE SDComponentID=67058;
UPDATE FCT_ShipDesignComponents SET Name='Cargo Hold - Large' WHERE SDComponentID=67059;
UPDATE FCT_ShipDesignComponents SET Name='Troop Transport Boarding Bay - Very Small' WHERE SDComponentID=67060;
UPDATE FCT_ShipDesignComponents SET Name='Maintenance Storage Bay' WHERE SDComponentID=76178;
UPDATE FCT_ShipDesignComponents SET Name='Small Maintenance Storage Bay' WHERE SDComponentID=76179;
UPDATE FCT_ShipDesignComponents SET Name='Tiny Maintenance Storage Bay' WHERE SDComponentID=76181;
UPDATE FCT_ShipDesignComponents SET Name='Troop Transport Bay - Small' WHERE SDComponentID=78585;
UPDATE FCT_ShipDesignComponents SET Name='Troop Transport Drop Bay - Small' WHERE SDComponentID=78586;
UPDATE FCT_ShipDesignComponents SET Name='Cryogenic Transport - Large' WHERE SDComponentID=78588;
UPDATE FCT_ShipDesignComponents SET Name='Troop Transport Bay - Very Large' WHERE SDComponentID=78589;
UPDATE FCT_ShipDesignComponents SET Name='Troop Transport Drop Bay - Very Large' WHERE SDComponentID=78590;
UPDATE FCT_ShipDesignComponents SET Name='Fighter Maintenance Storage Bay' WHERE SDComponentID=82471;
UPDATE FCT_ShipDesignComponents SET Name='Troop Transport Bay - Conventional' WHERE SDComponentID=92177;
UPDATE FCT_ShipDesignComponents SET Name='Cryogenic Transport - Conventional' WHERE SDComponentID=92178;